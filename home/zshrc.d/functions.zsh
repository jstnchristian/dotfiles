# URL Expansion from Tiny
expand_url() {
    curl --silent --head $1 | sed -n 's/Location: *//p'
}

generate_pass() {
    base64 /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c32
}

set-title() {
    echo -e "\e]0;$*\007"
}

ssh() {
    if pgrep kitty 2>&1 >/dev/null; then
        echo "Meow\!"
        command kitty +kitten ssh "$@"
    elif command -v ssh.exe >/dev/null; then
        ssh.exe "$@"
    else
        command ssh "$@"
    fi
}

timezsh() {
    shell=${1-$SHELL}
    for i in $(seq 1 10); do /usr/bin/time $shell -i -c exit; done
}

sendterm() {
    infocmp -x | ssh "$1" -- tic -x -
}

libbydl ()
{
    /usr/bin/java \
        -jar /home/justinmc/LibbyDownload-3.2/LibbyDownload.jar \
        -har /srv/hoard/files/har/libbyapp.com.har \
        -c \
        -ffmpeg /bin/ffmpeg \
        -out "$1"
}
