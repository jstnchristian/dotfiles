ZSH_THEME=""

if [[ -x "$HOME/.1password/agent.sock" ]]; then
	export SSH_AUTH_SOCK=$HOME/.1password/agent.sock
fi

source "$HOME/.zshrc.d/functions.zsh"
source "$HOME/.zshrc.d/paths.zsh"
source "$HOME/.zshrc.d/alias.zsh"
source "$HOME/.zshrc.d/autocomplete.zsh"
source "$HOME/.zshrc.d/oh-my-zsh.zsh"
