source $HOME/.zshrc.d/functions.zsh
source $HOME/.zshrc.d/paths.zsh
source $HOME/.zshrc.d/alias.zsh
source $HOME/.zshrc.d/autocomplete.zsh
source $HOME/.zshrc.d/oh-my-zsh.zsh

# Mac CA Bundle
export REQUESTS_CA_BUNDLE="$HOME/.mac-ca-roots"

# Mac specific exports
export TERMINFO_DIRS=$TERMINFO_DIRS:$HOME/.local/share/terminfo

# macos-specific aliases
alias ssh="kitty +kitten ssh"
alias workspace='cd /Volumes/workspace'

# # If on mac, need to ssh-add
# if ! ssh-add -L | grep -q "chrijust@"
# then
#     ssh-add -D && ssh-add
# fi
