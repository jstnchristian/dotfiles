# Editor
if command -v lvim >/dev/null; then
	alias edit="lvim"
elif command -v nvim >/dev/null; then
	alias edit="nvim"
elif command -v vim >/dev/null; then
	alias edit="vim"
else
	alias edit="vi"
fi

# Kubectl
if command -v minikube >/dev/null; then
	alias kubectl="minikube kubectl --"
fi
